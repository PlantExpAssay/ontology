The [ontology](https://bitbucket.org/PlantExpAssay/ontology/) project of the [PlantExpAssay](https://bitbucket.org/PlantExpAssay/) team contains the all files related to the development of the **Plant Experimental Assay Ontology**.

### Description ###

The Plant domain has been the subject of several attempts to structure and formally define terms and corresponding relations, such as their anatomical features, developmental stages, and the application of particular experimental procedures to a biological problem. However, a focus on experimental assays in order to describe the whole experimental procedure, to the best of our knowledge, has only been attempted in the context of a very general description based on classical views of the scientific method.
        In this study, we focus on the development and proposal of an ontology dedicated to the description of these experimental procedures, regardless of the scientific questions that prompted the assays. This ontology includes entities from three distinct realms (biological, physical and data), which include both experimental products, their relations and the protocols describing their manipulation.
        The final outcome is a useful and comprehensive ontology in the plant domain, that will be useful for data integration and querying heterogeneous data.

### Publication ###

Poster

* N.D. Mendes, P.T. Monteiro, C. Vaz, I. Chaves: Towards a Plant Experimental Assay Ontology. 10th International Conference on Data Integration in the Life Sciences (DILS). 2014

### Acknowledgements ###

This project was funded by the DATASTORM (Large-Scale Data Management in Cloud Environments) project ref. EXCL/EEI-ESS/0257/2012, from the Portuguese [FCT](http://www.fct.pt).

The team working on the project:

* Inês Chaves
* Daniel Faria
* Pedro T. Monteiro
* Nuno D. Mendes
* Cátia Vaz